# Drexel Digital Media Attendance Application

With incoming class sizes growing, it has been increasingly more difficult to take attendance in a swift manner.

This application utilizes the student ID that every student carries to check-in a student.

Unit tests are also included.

## Changing the application name
To change the application name that appears across the application, change the `APP_NAME` environment variable.

The following files should be adjusted so that the default values reflect the adjusted application name:
* config
    * app.php
* resources/views
    * home.blade.php
    * auth/login.blade.php
    * layouts/app.blade.php

## Running unit tests
From the project root directory, run the following:
```
php vendor/phpunit/phpunit/phpunit [optional_test_files]
```

## Requirements
* [Composer](https://getcomposer.org/)
* MySQL
* [PHP >= 7.1.2](https://php.net)
    * OpenSSL PHP Extension
    * PDO PHP Extension
    * Mbstring PHP Extension
    * Tokenizer PHP Extension
    * XML PHP Extension
    * PHPUnit

## Tested using
* Composer 1.6.3
* Laravel 5.6.11
* MySQL 5.7
* PHP 7.2.2
  * PHPUnit 7.0.2
