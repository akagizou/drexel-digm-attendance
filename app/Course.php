<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Course
 * @package App
 */
class Course extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'monday' => 'boolean',
        'tuesday' => 'boolean',
        'wednesday' => 'boolean',
        'thursday' => 'boolean',
        'friday' => 'boolean',
        'expired' => 'boolean',
        'not_expired' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'crn',
        'subject_code',
        'course_number',
        'section',
        'title',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'timezone',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'course_term_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Get the course attendance records associated with the course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseAttendances()
    {
        return $this->hasMany(CourseAttendance::class);
    }

    /**
     * Get the course term for the course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function courseTerm()
    {
        return $this->belongsTo(CourseTerm::class);
    }

    /**
     * Get the students associated with the course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function students()
    {
        return $this->belongsToMany(Student::class);
    }

    /**
     * Get the instructors (users) associated with the course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function instructors()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get the course's code as a string.
     *
     * Format:
     *      [subject_code] [course_number]-[section]
     * Example:
     *      DIGM 101-001
     *
     * @return string
     */
    public function getCourseCodeAttribute()
    {
        return $this->subject_code . ' ' . $this->course_number . '-' . $this->section;
    }

    /**
     * Get the course's start time in 12-hour format with the meridian.
     *
     * @return string
     */
    public function getStartTime12Attribute()
    {
        return Carbon::parse($this->start_time)->format('h:i A');
    }

    /**
     * Get the course's end time in 12-hour format with the meridian.
     *
     * @return string
     */
    public function getEndTime12Attribute()
    {
        return Carbon::parse($this->end_time)->format('h:i A');
    }

    /**
     * Get the course's start time in 24-hour format without the seconds.
     *
     * @return string
     */
    public function getStartTime24Attribute()
    {
        return Carbon::parse($this->start_time)->format('H:i');
    }

    /**
     * Get the course's end time in 24-hour format without the seconds.
     *
     * @return string
     */
    public function getEndTime24Attribute()
    {
        return Carbon::parse($this->end_time)->format('H:i');
    }

    /**
     * Get a boolean if the course is past the end date.
     *
     * @return bool
     */
    public function getExpiredAttribute()
    {
        return Carbon::today()->timezone($this->timezone) > Carbon::parse($this->end_date);
    }

    /**
     * Get a boolean if the course is not past the end date.
     *
     * @return bool
     */
    public function getNotExpiredAttribute()
    {
        return Carbon::today()->timezone($this->timezone) <= Carbon::parse($this->end_date);
    }

    /**
     * Get the courses that have an end date before today, exclusively.
     * Query scope for chaining.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExpired($query)
    {
        return $query->whereDate('end_date', '<', Carbon::today());
    }

    /**
     * Get the courses that have an end date at or after today.
     * Query scope for chaining.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotExpired($query)
    {
        return $query->whereDate('end_date', '>=', Carbon::today());
    }
}
