<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CourseTerm
 * @package App
 */
class CourseTerm extends Model
{
    /**
     * Get all courses that belong to a term.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
}
