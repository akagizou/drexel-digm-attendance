<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Role;
use App\User;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::notSelf()
            ->orderBy('active', 'desc')
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->with('roles')
            ->get();

        return view('users.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('users.edit', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $authenticatedUser = auth()->user();

        $courseData = [
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'username' => request('username'),
            'password' => bcrypt(request('password')),
            'created_by' => $authenticatedUser->id,
            'updated_by' => $authenticatedUser->id,
        ];

        $user = User::create($courseData);

        if ($user) {
            session()->flash('success', $user->full_name_and_username . ' has been successfully added');

            $user->roles()->sync(request('roles'));

            return redirect(route('users.index'));
        } else {
            session()->flash('error', 'Unable to add user');

            return redirect(route('users.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $userId
     *
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $user = User::with(['courses' => function ($query) {
            $query->notExpired()->orderBy('start_time')->orderBy('start_date');
        }])->find($userId);

        return view('users.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $authenticatedUser = auth()->user();

        if ($authenticatedUser->id == $id) {
            // Editing themselves so redirect to the profile edit page.
            return redirect(route('profiles.edit'));
        }

        $user = User::find($id);
        if (is_null($user)) {
            session()->flash('error', 'User could not be found');

            return back();
        }

        $roles = Role::all();

        return view('users.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRequest  $request
     * @param  int          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $authenticatedUser = auth()->user();

        $user = User::find($id);
        if (is_null($user)) {
            session()->flash('error', 'User could not be found');

            return back();
        }

        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->email = request('email');
        $user->username = request('username');
        $user->updated_by = $authenticatedUser->id;

        // Only update the password if it is re-entered.
        if ($request->filled('password')) {
            $user->password = bcrypt(request('password'));
        }

        if ($user->save()) {
            session()->flash('success', $user->full_name_and_username . ' has been successfully updated');

            $user->roles()->sync(request('roles'));

            return redirect(route('users.index'));
        } else {
            session()->flash('error', 'Unable to update ' . $user->full_name_and_username);

            return redirect(route('users.edit', ['user' => $user->id]));
        }
    }

    /**
     * Reactivate the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function reactivate($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (is_null($user)) {
            session()->flash('error', 'User could not be found');

            return back();
        }

        if (auth()->user()->id == $id) {
            session()->flash('error', 'You cannot reactivate yourself');

            return redirect(route('users.index'));
        }

        if ($user->reactivate()) {
            session()->flash('success', $user->full_name_and_username . ' has been successfully reactivated');
        } else {
            session()->flash('error', 'Unable to reactivate ' . $user->full_name_and_username);
        }

        return redirect(route('users.index'));
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deactivate($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (is_null($user)) {
            session()->flash('error', 'User could not be found');

            return back();
        }

        if (auth()->user()->id == $id) {
            session()->flash('error', 'You cannot deactivate yourself');

            return redirect(route('users.index'));
        }

        if ($user->deactivate()) {
            session()->flash('success', $user->full_name . ' (' . $user->username . ') has been successfully deactivated');
        } else {
            session()->flash('error', 'Unable to deactivate ' . $user->full_name . ' (' . $user->username . ')');
        }

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            session()->flash('error', 'User could not be found');

            return back();
        }

        if (auth()->user()->id == $id) {
            session()->flash('error', 'You cannot delete yourself');

            return redirect(route('users.index'));
        }

        if ($user->delete()) {
            session()->flash('success', 'User has been successfully removed');
        } else {
            session()->flash('error', 'Unable to remove user');
        }

        return redirect(route('users.index'));
    }
}
