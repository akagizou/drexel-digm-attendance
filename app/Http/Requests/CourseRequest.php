<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CourseRequest
 *
 * @package App\Http\Requests
 */
class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crn' => [
                'required',
                'integer',
                Rule::unique('courses')->ignore($this->course),
            ],
            'subject_code' => 'required|string|max:255',
            'course_number' => 'required|string|max:255',
            'section' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'start_date' => 'required|date_format:Y-m-d|before:end_date',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'start_time' => 'required|date_format:H:i|before:end_time',
            'end_time' => 'required|date_format:H:i|after:start_time',
            'timezone' => 'timezone',
            'monday' => 'nullable|boolean|required_without_all:tuesday,wednesday,thursday,friday',
            'tuesday' => 'nullable|boolean|required_without_all:monday,wednesday,thursday,friday',
            'wednesday' => 'nullable|boolean|required_without_all:monday,tuesday,thursday,friday',
            'thursday' => 'nullable|boolean|required_without_all:monday,tuesday,wednesday,friday',
            'friday' => 'nullable|boolean|required_without_all:monday,tuesday,wednesday,thursday',
            'course_term_id' => 'required|integer|exists:course_terms,id',
            'instructors.*' => 'required|integer|exists:users,id',
            'students.*' => 'integer|exists:students,id',
        ];
    }
}
