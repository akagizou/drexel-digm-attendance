<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StudentRequest
 *
 * @package App\Http\Requests
 */
class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $studentId = $this->student;

        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'university_id' => [
                'required',
                'integer',
                Rule::unique('students')->ignore($studentId),
            ],
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('students')->ignore($studentId),
            ],
        ];
    }
}
