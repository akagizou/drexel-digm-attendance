<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Student
 *
 * @package App
 */
class Student extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'university_id',
        'email',
    ];

    /**
     * Get the courses associated with a student.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * Get the course attendance statuses associated with a student.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseAttendances()
    {
        return $this->hasMany(CourseAttendance::class);
    }

    /**
     * Set the user to inactive and save it to the database.
     *
     * @return bool
     */
    public function deactivate()
    {
        $this->active = false;

        return $this->save();
    }

    /**
     * Set the user to active and save it to the database.
     *
     * @return bool
     */
    public function reactivate()
    {
        $this->active = true;

        return $this->save();
    }

    /**
     * Get the student's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Return a human-readable Yes/No string for the active flag.
     *
     * @return string
     */
    public function getIsActiveReadableAttribute()
    {
        return $this->active ? 'Yes' : 'No';
    }

    /**
     * Get the students that are active.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
