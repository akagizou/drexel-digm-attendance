<?php

use Faker\Generator as Faker;

$factory->define(\App\CourseAttendance::class, function (Faker $faker) {
    $user = factory(App\User::class)->create();

    return [
        'course_id' => function () {
            return factory(App\Course::class)->create()->id;
        },
        'student_id' => function () {
            return factory(App\Student::class)->create()->id;
        },
        'attendance_status_id' => function () {
            return factory(App\AttendanceStatus::class)->create()->id;
        },
        'excused' => $faker->boolean(20),
        'note' => $faker->paragraph(),
        'created_by' => $user->id,
        'updated_by' => $user->id,
    ];
});
