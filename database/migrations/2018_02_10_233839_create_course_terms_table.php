<?php

use App\CourseTerm;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCourseTermsTable
 */
class CreateCourseTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->unsignedInteger('display_order')->unique();
            $table->timestamps();
        });

        factory(CourseTerm::class)->create([
            'name' => 'Fall',
            'display_order' => 1,
        ]);

        factory(CourseTerm::class)->create([
            'name' => 'Winter',
            'display_order' => 2,
        ]);

        factory(CourseTerm::class)->create([
            'name' => 'Spring',
            'display_order' => 3,
        ]);

        factory(CourseTerm::class)->create([
            'name' => 'Summer',
            'display_order' => 4,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_terms');
    }
}
