@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel-heading">Take Attendance - {{ \Carbon\Carbon::today()->toFormattedDateString() }}</div>

        <div>
            @include('layouts.flash')

            <form id="attendance-form"
                  class="take-attendance-manual-form"
                  method="POST"
                  action="{{ route('attendances.save', ['course' => $course->id]) }}">
                @method('PUT')
                @csrf

                <table class="table">
                    <thead>
                        <tr class="table-header-row">
                            <th class="table-header-cell dash-attendance-student-first-name">First Name</th>
                            <th class="table-header-cell dash-attendance-student-last-name">Last Name</th>
                            <th class="table-header-cell dash-attendance-student-email">Email Address</th>
                            <th class="table-header-cell dash-actions">Status</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if ($course->students->isNotEmpty())
                            @foreach ($course->students as $student)
                                <tr class="table-content-row">
                                    <td class="table-content-cell dash-attendance-student-first-name">{{ $student->first_name }}</td>

                                    <td class="table-content-cell dash-attendance-student-last-name">{{ $student->last_name }}</td>

                                    <td class="table-content-cell dash-attendance-student-email">{{ $student->email }}</td>

                                    <td class="table-content-cell">
                                        <div class="dash-actions">
                                            @foreach ($attendanceStatuses as $status)
                                                <div class="radio-container">
                                                    <input type="radio"
                                                           id="attendance-status-{{ $status->name }}-{{ $student->id }}"
                                                           class="radio radio-option"
                                                           name="attendances[{{ $student->id }}][status]"
                                                           value="{{ $status->id }}"
                                                           {{ (old('attendance[' . $student->id . '][status]') == $status->id) ||
                                                              ($student->courseAttendances->isEmpty() && $status->name == 'Absent') ||
                                                              ($student->courseAttendances->isNotEmpty() && $status->id == $student->courseAttendances->first()->attendance_status_id)
                                                                ? 'checked'
                                                                : '' }}>

                                                    <label for="attendance-status-{{ $status->name }}-{{ $student->id }}">{{ $status->name }}</label>
                                                </div>
                                            @endforeach

                                            <div class="checkbox-with-label-container">
                                                <input type="checkbox"
                                                       id="attendance-excused-{{ $student->id }}"
                                                       class="checkbox"
                                                       name="attendances[{{ $student->id }}][excused]"
                                                       value="1"
                                                       {{ old('attendance[' . $student->id . '][excused]') == 1 ||
                                                          ($student->courseAttendances->isNotEmpty() && $student->courseAttendances->first()->excused == 1)
                                                            ? 'checked'
                                                            : '' }}>

                                                <label for="attendance-excused-{{ $student->id }}">Excused</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="table-content-row">
                                <td class="table-content-cell" colspan="4">There are no students registered.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </form>

            <div class="form-submit-buttons">
                <button type="submit" form="attendance-form" class="btn btn-primary btn-submit" onclick="window.confirm('Are you sure you would like to submit today\'s attendance?');">Save</button>
            </div>
        </div>
    </div>
@endsection
