@extends('layouts.app')

@section('content')
<div class="container">
    <div class="form-heading-container">
        <img src="{{ asset('img/westphal-logo.png') }}" alt="Westphal Logo" class="guest-logo">
        <div class="panel-heading">{{ config('app.name', 'DIGM Attendance') }} Login</div>
    </div>

    <div>
        <form class="guest-form shadow" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="control-label">Username</label>

                <div class="form-field">
                    <input type="text"
                           id="username"
                           class="form-control guest-input"
                           name="username"
                           value="{{ old('username') }}"
                           placeholder="abc123"
                           required
                           autofocus>

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>

                <div class="form-field">
                    <input type="password"
                           id="password"
                           class="form-control guest-input"
                           name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group remember-me-field">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>
            </div>

            <div class="form-submit-buttons">
                <a class="btn-submit" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>

                <button type="submit" class="btn btn-primary btn-submit">
                    Login
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
