@extends('layouts.app')

@section('content')
<div class="container">
    <div class="form-heading-container">
        <img src="{{ asset('img/westphal-logo.png') }}" alt="Westphal Logo" class="guest-logo">
        <div class="panel-heading">Reset Password</div>
    </div>

    <div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="guest-form shadow" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} single-field">
                <label for="email" class="control-label">E-Mail Address</label>

                <div class="form-field">
                    <input type="email"
                           id="email"
                           class="form-control guest-input"
                           name="email"
                           value="{{ old('email') }}"
                           placeholder="example@example.com"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-submit-buttons">
                <a class="btn btn-primary btn-link btn-submit" href="{{ route('home') }}">
                    Cancel
                </a>

                <button type="submit" class="btn btn-primary btn-submit">
                    Send Reset Link
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
