@extends('layouts.app')

@section('content')
<div class="container">
    <div class="form-heading-container">
        <img src="{{ asset('img/westphal-logo.png') }}" alt="Westphal Logo" class="guest-logo">
        <div class="panel-heading">Reset Password</div>
    </div>

    <div>
        <form class="guest-form shadow" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail Address</label>

                <div class="form-field">
                    <input type="email"
                           id="email"
                           class="form-control guest-input"
                           name="email"
                           value="{{ $email or old('email') }}"
                           placeholder="example@example.com"
                           required
                           autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>

                <div class="form-field">
                    <input type="password"
                           id="password"
                           class="form-control guest-input"
                           name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label">Confirm Password</label>

                <div class="form-field">
                    <input type="password"
                           id="password-confirm"
                           class="form-control guest-input"
                           name="password_confirmation"
                           required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="form-submit-buttons">
                    <button type="submit" class="btn btn-primary btn-submit">
                        Reset Password
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
