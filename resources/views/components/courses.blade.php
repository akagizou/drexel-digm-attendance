<table class="table dash-course-table">
    <caption class="table-title">{{ $dashTitle }}</caption>

    <thead>
        <tr class="table-header-row">
            <th class="table-header-cell dash-course-title">Course Title</th>
            <th class="table-header-cell dash-meeting-days">Meeting Days</th>
            <th class="table-header-cell dash-start-date">Start Date</th>
            <th class="table-header-cell dash-end-date">End Date</th>
            <th class="table-header-cell dash-start-time">Start Time</th>
            <th class="table-header-cell dash-end-time">End Time</th>
            <th class="table-header-cell dash-actions">
                {{ $dashHeaderActions }}
            </th>
        </tr>
    </thead>

    <tbody>
    @if ($courses->isNotEmpty())
        @foreach ($courses as $course)
            <tr class="table-content-row">
                <td class="table-content-cell dash-course-title">
                    <abbr class="abbr-tooltip" title="{{ $course->title }}">
                        <p>{{ $course->course_code }}</p>
                        <p>{{ str_limit($course->title, $limit = 40, $end = '...') }}</p>
                    </abbr>
                </td>

                <td class="table-content-cell dash-meeting-days">
                    <ul>
                        @if ($course->monday)
                            <li>Monday</li>
                        @endif
                        @if ($course->tuesday)
                            <li>Tuesday</li>
                        @endif
                        @if ($course->wednesday)
                            <li>Wednesday</li>
                        @endif
                        @if ($course->thursday)
                            <li>Thursday</li>
                        @endif
                        @if ($course->friday)
                            <li>Friday</li>
                        @endif
                    </ul>
                </td>

                <td class="table-content-cell dash-start-date">{{ $course->start_date }}</td>

                <td class="table-content-cell dash-end-date">{{ $course->end_date }}</td>

                <td class="table-content-cell dash-start-time">{{ $course->start_time_12 }}</td>

                <td class="table-content-cell dash-end-time">{{ $course->end_time_12 }}</td>

                <td class="table-content-cell">
                    <div class="dash-actions">
                        @if ($course->not_expired && $course->students->isNotEmpty())
                            <a href="{{ route('attendances.create', ['course' => $course->id]) }}" class="btn btn-primary btn-link btn-dash-action">Take Attendance</a>
                        @endif
                        <a href="{{ route('courses.show', ['course' => $course->id]) }}" class="btn btn-primary btn-link btn-dash-action">Details</a>
                        <a href="{{ route('courses.edit', ['course' => $course->id]) }}" class="btn btn-primary btn-link btn-dash-action">Edit</a>
                    </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr class="table-content-row">
            <td class="table-content-cell" colspan="7">You currently have no classes.</td>
        </tr>
    @endif
    </tbody>
</table>
