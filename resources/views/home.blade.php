@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel-heading">{{ config('app.name', 'DIGM Attendance') }}</div>

    <div>
        @include('layouts.flash')

        @component('components.courses', ['courses' => $courses])
            @slot('dashTitle')
                Current Classes
            @endslot

            @slot('dashHeaderActions')
                <a href="{{ route('courses.create') }}" class="btn btn-primary btn-link">Add Class</a>
            @endslot
        @endcomponent

        @if ($expiredCourses->isNotEmpty())
            @component('components.courses', ['courses' => $expiredCourses])
                @slot('dashTitle')
                    Expired Classes
                @endslot

                @slot('dashHeaderActions')
                @endslot
            @endcomponent
        @endif
    </div>
</div>
@endsection
