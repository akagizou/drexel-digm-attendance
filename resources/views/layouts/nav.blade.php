<nav class="navbar navbar-static-top">
    @auth
    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav">
        <li class="navbar-link-item navbar-brand-container">
            <!-- Branding Image -->
            <a href="{{ route('home') }}" class="navbar-brand navbar-link">
                <img src="{{ asset('img/westphal-logo.png') }}" alt="Westphal Logo" class="navbar-logo">
            </a>
        </li>
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav navbar-right">
        <li class="navbar-link-item navbar-text-container">
            <a href="{{ route('courses.index') }}" class="navbar-link navbar-text">
                Courses
            </a>
        </li>

        <li class="navbar-link-item navbar-text-container">
            <a href="{{ route('students.index') }}" class="navbar-link navbar-text">
                Students
            </a>
        </li>

        @if (auth()->user()->is_admin)
            <li class="navbar-link-item navbar-text-container">
                <a href="{{ route('users.index') }}" class="navbar-link navbar-text">
                    Admin
                </a>
            </li>
        @endif

        <li class="navbar-link-item navbar-text-container">
            <a href="{{ route('profiles.edit') }}" class="navbar-link navbar-text">
                {{ auth()->user()->full_name }}
            </a>
        </li>

        <li class="navbar-link-item">
            <button form="logout-form"
                    class="navbar-link btn btn-primary">
                Logout
            </button>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
    @endauth
</nav>
