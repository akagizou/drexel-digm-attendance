<?php

namespace Tests\Feature;

use App\AttendanceStatus;
use App\Course;
use App\CourseAttendance;
use App\Student;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class AttendanceRouteTest
 *
 * @package Tests\Feature
 */
class AttendanceRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Assert that the manual attendance page can be accessed when authorized.
     */
    public function testAccessTakeAttendancePageAuthorized()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        $response = $this->get(route('attendances.create', ['course' => $course->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the take attendance page can be accessed even though attendance was partially taken.
     */
    public function testAccessTakeAttendancePageWhenPartialAttendanceTaken()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[0]->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[1]->id,
        ]);

        $response = $this->get(route('attendances.create', ['course' => $course->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the manual attendance page cannot be accessed when unauthorized.
     */
    public function testAccessTakeAttendancePageUnauthorized()
    {
        $user = factory(User::class)->create();

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        $response = $this->get(route('attendances.create', ['course' => $course->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that the manual attendance page cannot be accessed with an invalid course ID.
     */
    public function testAccessTakeAttendancePageWithInvalidCourseId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('attendances.create', ['course' => 0]));
        $response->assertRedirect();
    }

    /**
     * Assert that the manual attendance page cannot be accessed when no students are registered for the course.
     *
     * @return void
     */
    public function testAccessTakeAttendancePageWithNoStudentsRegistered()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $response = $this->get(route('attendances.create', ['course' => $course->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that the take attendance page can be accessed even though everyone's attendance was taken.
     */
    public function testAccessTakeAttendancePageWhenFullAttendanceTaken()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[0]->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[1]->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[2]->id,
        ]);

        $response = $this->get(route('attendances.create', ['course' => $course->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that a student's attendance record for a class can be accessed when authorized.
     */
    public function testAccessStudentAttendancesAuthorized()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $student = factory(Student::class)->create();
        $course->students()->attach($student);

        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
        ]);

        $response = $this->get(route('attendances.students.edit', ['course' => $course->id, 'student' => $student->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that a student's attendance record cannot be accessed when unauthorized and is instead redirected.
     */
    public function testAccessStudentAttendancesUnauthorized()
    {
        $user = factory(User::class)->create();

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $student = factory(Student::class)->create();
        $course->students()->attach($student);

        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
        ]);

        $response = $this->get(route('attendances.students.edit', ['course' => $course->id, 'student' => $student->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that a student's attendance record cannot be accessed with a user not associated with the course.
     */
    public function testAccessStudentAttendancesAsIrrelevantInstructor()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $response = $this->get(route('attendances.students.edit', ['course' => $course->id, 'student' => 0]));
        $response->assertRedirect();
    }

    /**
     * Assert that a student's attendance record cannot be accessed with an invalid course ID.
     */
    public function testAccessStudentAttendancesWithInvalidCourseId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();

        $response = $this->get(route('attendances.students.edit', ['course' => 0, 'student' => $student->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that a student's attendance record cannot be accessed with an invalid student ID.
     */
    public function testAccessStudentAttendancesWithInvalidStudentId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $response = $this->get(route('attendances.students.edit', ['course' => $course->id, 'student' => 0]));
        $response->assertRedirect();
    }

    /**
     * Assert that a student's attendance record cannot be accessed with a student ID that does not belong to the course.
     */
    public function testAccessStudentAttendancesWithStudentIdNotBelongingToCourse()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $student = factory(Student::class)->create();

        $response = $this->get(route('attendances.students.edit', ['course' => $course->id, 'student' => $student->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that the Take Attendance page can store new attendance records for today.
     */
    public function testAddAttendanceAllNewRecordsFromTakeAttendance()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        $attendanceStatuses = factory(AttendanceStatus::class, 2)->create();

        $data = [
            'attendances' => [
                $students[0]->id => [
                    'status' => $attendanceStatuses[0]->id,
                ],
                $students[1]->id => [
                    'status' => $attendanceStatuses[0]->id,
                    'excused' => true,
                ],
                $students[2]->id => [
                    'status' => $attendanceStatuses[1]->id,
                    'note' => 'This student had a note from a doctor',
                ],
            ],
        ];

        $response = $this->put(route('attendances.save', ['course' => $course->id]), $data);
        $response->assertRedirect();

        $statusIds = [];
        foreach ($attendanceStatuses as $status) {
            $statusIds[] = $status->id;
        }

        $fetchedAttendanceRecords = CourseAttendance::whereIn('attendance_status_id', $statusIds)->get();
        $this->assertCount(3, $fetchedAttendanceRecords);

        $fetchedAttendance1 = CourseAttendance::where('course_id', $course->id)->where('student_id', $students[0]->id)->today()->first();
        $this->assertEquals($course->id, $fetchedAttendance1->course_id);
        $this->assertEquals($students[0]->id, $fetchedAttendance1->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance1->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance1->excused);
        $this->assertEquals(null, $fetchedAttendance1->note);
        $this->assertEquals($user->id, $fetchedAttendance1->created_by);
        $this->assertEquals($user->id, $fetchedAttendance1->updated_by);

        $fetchedAttendance2 = CourseAttendance::where('course_id', $course->id)->where('student_id', $students[1]->id)->today()->first();
        $this->assertEquals($course->id, $fetchedAttendance2->course_id);
        $this->assertEquals($students[1]->id, $fetchedAttendance2->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance2->attendance_status_id);
        $this->assertEquals(true, $fetchedAttendance2->excused);
        $this->assertEquals(null, $fetchedAttendance2->note);
        $this->assertEquals($user->id, $fetchedAttendance2->created_by);
        $this->assertEquals($user->id, $fetchedAttendance2->updated_by);

        $fetchedAttendance3 = CourseAttendance::where('course_id', $course->id)->where('student_id', $students[2]->id)->today()->first();
        $this->assertEquals($course->id, $fetchedAttendance3->course_id);
        $this->assertEquals($students[2]->id, $fetchedAttendance3->student_id);
        $this->assertEquals($attendanceStatuses[1]->id, $fetchedAttendance3->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance3->excused);
        $this->assertEquals('This student had a note from a doctor', $fetchedAttendance3->note);
        $this->assertEquals($user->id, $fetchedAttendance3->created_by);
        $this->assertEquals($user->id, $fetchedAttendance3->updated_by);
    }

    /**
     * Assert that the Take Attendance page can store and update attendance records for today.
     */
    public function testAddAttendanceSomeNewRecordsFromTakeAttendance()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        $attendanceStatuses = factory(AttendanceStatus::class, 2)->create();

        $courseAttendance1 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[0]->id,
            'attendance_status_id' => $attendanceStatuses[1],
            'created_by' => $user->id,
        ]);
        $courseAttendance2 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[1]->id,
            'attendance_status_id' => $attendanceStatuses[1],
            'created_by' => $user->id,
        ]);

        $data = [
            'attendances' => [
                $students[0]->id => [
                    'status' => $attendanceStatuses[0]->id,
                    'excused' => true,
                ],
                $students[1]->id => [
                    'status' => $attendanceStatuses[0]->id,
                ],
                $students[2]->id => [
                    'status' => $attendanceStatuses[1]->id,
                ],
            ],
        ];

        $response = $this->put(route('attendances.save', ['course' => $course->id]), $data);
        $response->assertRedirect();

        $statusIds = [];
        foreach ($attendanceStatuses as $status) {
            $statusIds[] = $status->id;
        }

        $fetchedAttendanceRecords = CourseAttendance::whereIn('attendance_status_id', $statusIds)->get();
        $this->assertCount(3, $fetchedAttendanceRecords);

        $fetchedAttendance1 = CourseAttendance::find($courseAttendance1->id);
        $this->assertEquals($course->id, $fetchedAttendance1->course_id);
        $this->assertEquals($students[0]->id, $fetchedAttendance1->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance1->attendance_status_id);
        $this->assertEquals(true, $fetchedAttendance1->excused);
        $this->assertEquals($courseAttendance1->note, $fetchedAttendance1->note);
        $this->assertEquals($user->id, $fetchedAttendance1->created_by);
        $this->assertEquals($user->id, $fetchedAttendance1->updated_by);

        $fetchedAttendance2 = CourseAttendance::find($courseAttendance2->id);
        $this->assertEquals($course->id, $fetchedAttendance2->course_id);
        $this->assertEquals($students[1]->id, $fetchedAttendance2->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance2->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance2->excused);
        $this->assertEquals($courseAttendance2->note, $fetchedAttendance2->note);
        $this->assertEquals($user->id, $fetchedAttendance2->created_by);
        $this->assertEquals($user->id, $fetchedAttendance2->updated_by);

        $fetchedAttendance3 = CourseAttendance::where('course_id', $course->id)->where('student_id', $students[2]->id)->today()->first();
        $this->assertEquals($course->id, $fetchedAttendance3->course_id);
        $this->assertEquals($students[2]->id, $fetchedAttendance3->student_id);
        $this->assertEquals($attendanceStatuses[1]->id, $fetchedAttendance3->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance3->excused);
        $this->assertEquals(null, $fetchedAttendance3->note);
        $this->assertEquals($user->id, $fetchedAttendance3->created_by);
        $this->assertEquals($user->id, $fetchedAttendance3->updated_by);
    }

    /**
     * Assert that the Take Attendance page can update attendance records for today.
     */
    public function testUpdateAttendanceNoNewRecordsFromTakeAttendance()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $students = factory(Student::class, 3)->create();
        $course->students()->attach($students);

        $attendanceStatuses = factory(AttendanceStatus::class, 2)->create();

        $courseAttendance1 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[0]->id,
            'attendance_status_id' => $attendanceStatuses[1],
            'created_by' => $user->id,
        ]);
        $courseAttendance2 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[1]->id,
            'attendance_status_id' => $attendanceStatuses[1],
            'created_by' => $user->id,
        ]);
        $courseAttendance3 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $students[2]->id,
            'attendance_status_id' => $attendanceStatuses[0],
            'created_by' => $user->id,
        ]);

        $data = [
            'attendances' => [
                $students[0]->id => [
                    'status' => $attendanceStatuses[0]->id,
                ],
                $students[1]->id => [
                    'status' => $attendanceStatuses[0]->id,
                ],
                $students[2]->id => [
                    'status' => $attendanceStatuses[1]->id,
                    'excused' => true,
                    'note' => 'Sports excursion',
                ],
            ],
        ];

        $response = $this->put(route('attendances.save', ['course' => $course->id]), $data);
        $response->assertRedirect();

        $statusIds = [];
        foreach ($attendanceStatuses as $status) {
            $statusIds[] = $status->id;
        }

        $fetchedAttendanceRecords = CourseAttendance::whereIn('attendance_status_id', $statusIds)->get();
        $this->assertCount(3, $fetchedAttendanceRecords);

        $fetchedAttendance1 = CourseAttendance::find($courseAttendance1->id);
        $this->assertEquals($course->id, $fetchedAttendance1->course_id);
        $this->assertEquals($students[0]->id, $fetchedAttendance1->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance1->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance1->excused);
        $this->assertEquals($courseAttendance1->note, $fetchedAttendance1->note);
        $this->assertEquals($user->id, $fetchedAttendance1->created_by);
        $this->assertEquals($user->id, $fetchedAttendance1->updated_by);

        $fetchedAttendance2 = CourseAttendance::find($courseAttendance2->id);
        $this->assertEquals($course->id, $fetchedAttendance2->course_id);
        $this->assertEquals($students[1]->id, $fetchedAttendance2->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance2->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance2->excused);
        $this->assertEquals($courseAttendance2->note, $fetchedAttendance2->note);
        $this->assertEquals($user->id, $fetchedAttendance2->created_by);
        $this->assertEquals($user->id, $fetchedAttendance2->updated_by);

        $fetchedAttendance3 = CourseAttendance::find($courseAttendance3->id);
        $this->assertEquals($course->id, $fetchedAttendance3->course_id);
        $this->assertEquals($students[2]->id, $fetchedAttendance3->student_id);
        $this->assertEquals($attendanceStatuses[1]->id, $fetchedAttendance3->attendance_status_id);
        $this->assertEquals(true, $fetchedAttendance3->excused);
        $this->assertEquals('Sports excursion', $fetchedAttendance3->note);
        $this->assertEquals($user->id, $fetchedAttendance3->created_by);
        $this->assertEquals($user->id, $fetchedAttendance3->updated_by);
    }

    /**
     * Assert that the student's course attendance record edit page can update attendance records.
     */
    public function testUpdateStudentAttendancesForCourseWithValidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = factory(Course::class)->create();
        $course->instructors()->attach($user);

        $student = factory(Student::class)->create();
        $course->students()->attach($student);

        $attendanceStatuses = factory(AttendanceStatus::class, 2)->create();

        $courseAttendance1 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
            'attendance_status_id' => $attendanceStatuses[1],
            'created_by' => $user->id,
        ]);
        $courseAttendance2 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
            'attendance_status_id' => $attendanceStatuses[1],
            'note' => null,
            'created_by' => $user->id,
        ]);
        $courseAttendance3 = factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student->id,
            'attendance_status_id' => $attendanceStatuses[0],
            'created_by' => $user->id,
        ]);

        $data = [
            'attendances' => [
                $courseAttendance1->id => [
                    'status' => $attendanceStatuses[0]->id,
                    'note' => $courseAttendance1->note,
                ],
                $courseAttendance2->id => [
                    'status' => $attendanceStatuses[0]->id,
                    'note' => $courseAttendance2->note,
                ],
                $courseAttendance3->id => [
                    'status' => $attendanceStatuses[1]->id,
                    'excused' => true,
                    'note' => 'Sports excursion',
                ],
            ],
        ];

        $response = $this->patch(route('attendances.students.update', ['course' => $course->id, 'student' => $student->id]), $data);
        $response->assertRedirect();

        // These statuses are unique for this test so they can be used to get the new course attendance records.
        $statusIds = [];
        foreach ($attendanceStatuses as $status) {
            $statusIds[] = $status->id;
        }

        $fetchedAttendanceRecords = CourseAttendance::whereIn('attendance_status_id', $statusIds)->get();
        $this->assertCount(3, $fetchedAttendanceRecords);

        $fetchedAttendance1 = CourseAttendance::find($courseAttendance1->id);
        $this->assertEquals($course->id, $fetchedAttendance1->course_id);
        $this->assertEquals($student->id, $fetchedAttendance1->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance1->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance1->excused);
        $this->assertEquals($courseAttendance1->note, $fetchedAttendance1->note);
        $this->assertEquals($user->id, $fetchedAttendance1->created_by);
        $this->assertEquals($user->id, $fetchedAttendance1->updated_by);

        $fetchedAttendance2 = CourseAttendance::find($courseAttendance2->id);
        $this->assertEquals($course->id, $fetchedAttendance2->course_id);
        $this->assertEquals($student->id, $fetchedAttendance2->student_id);
        $this->assertEquals($attendanceStatuses[0]->id, $fetchedAttendance2->attendance_status_id);
        $this->assertEquals(false, $fetchedAttendance2->excused);
        $this->assertEquals(null, $fetchedAttendance2->note);
        $this->assertEquals($courseAttendance2->note, $fetchedAttendance2->note);
        $this->assertEquals($user->id, $fetchedAttendance2->created_by);
        $this->assertEquals($user->id, $fetchedAttendance2->updated_by);

        $fetchedAttendance3 = CourseAttendance::find($courseAttendance3->id);
        $this->assertEquals($course->id, $fetchedAttendance3->course_id);
        $this->assertEquals($student->id, $fetchedAttendance3->student_id);
        $this->assertEquals($attendanceStatuses[1]->id, $fetchedAttendance3->attendance_status_id);
        $this->assertEquals(true, $fetchedAttendance3->excused);
        $this->assertEquals('Sports excursion', $fetchedAttendance3->note);
        $this->assertEquals($user->id, $fetchedAttendance3->created_by);
        $this->assertEquals($user->id, $fetchedAttendance3->updated_by);
    }
}
