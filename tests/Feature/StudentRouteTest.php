<?php

namespace Tests\Feature;

use App\Student;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class StudentRouteTest
 * @package Tests\Feature
 */
class StudentRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Assert that the students list page can be accessed if authenticated.
     */
    public function testAccessListPageAuthenticated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('students.index'));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the student creation page can be accessed if authenticated.
     */
    public function testAccessCreatePageAuthenticated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('students.create'));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the student edit page can be accessed if authenticated.
     */
    public function testAccessEditPageAuthenticated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();

        $response = $this->get(route('students.edit', ['student' => $student->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that a student is created with valid form data.
     */
    public function testCreateStudentWithValidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => $studentDetails->email,
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a student is created with valid form data.
     */
    public function testUpdateStudentWithValidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();
        $newStudentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $newStudentDetails->first_name,
            'last_name' => $newStudentDetails->last_name,
            'university_id' => $newStudentDetails->university_id,
            'email' => $newStudentDetails->email,
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();

        $fetchedStudent = Student::find($student->id);
        $this->assertEquals($newStudentDetails->first_name, $fetchedStudent->first_name);
        $this->assertEquals($newStudentDetails->last_name, $fetchedStudent->last_name);
        $this->assertEquals($newStudentDetails->university_id, $fetchedStudent->university_id);
        $this->assertEquals($newStudentDetails->email, $fetchedStudent->email);
    }

    /**
     * Assert that a student is deactivated when the deactivate route is requested.
     */
    public function testDeactivateStudent()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();

        $response = $this->delete(route('students.deactivate', ['student' => $student->id]));
        $response->assertRedirect();

        $fetchedStudent = Student::find($student->id);
        $this->assertFalse((bool)$fetchedStudent->active);
    }

    /**
     * Assert that when the page is accessed as an unauthorized guest, it redirects the user.
     */
    public function testAccessListPageUnauthorized()
    {
        $response = $this->get(route('students.index'));
        $response->assertRedirect();
    }

    /**
     * Assert that when the page is accessed as an unauthorized guest, it redirects the user.
     */
    public function testAccessCreatePageUnauthorized()
    {
        $response = $this->get(route('students.create'));
        $response->assertRedirect();
    }

    /**
     * Assert that when the page is accessed as an unauthorized guest, it redirects the user.
     */
    public function testAccessUpdatePageUnauthorized()
    {
        $student = factory(Student::class)->create();

        $response = $this->get(route('students.edit', ['student' => $student->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that when the route is accessed as an unauthorized guest, it redirects the user.
     */
    public function testDeactivateStudentUnauthorized()
    {
        $student = factory(Student::class)->create();

        $response = $this->delete(route('students.deactivate', ['student' => $student->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that the edit page redirects to the student list page with an invalid ID.
     */
    public function testAccessEditPageWithNonExistentId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('students.edit', ['student' => 0]));
        $response->assertRedirect();
    }

    /**
     * Assert that the edit page redirects to the student list page with an invalid ID.
     */
    public function testUpdateStudentWithInvalidId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->patch(route('students.update', ['student' => 0]));
        $response->assertRedirect();
    }

    /**
     * Assert that the edit page redirects to the student list page with an invalid ID.
     */
    public function testDeactivateStudentWithInvalidId()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->delete(route('students.deactivate', ['student' => 0]));
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with a blank first name.
     */
    public function testCreateStudentWithBlankFirstNameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => '',
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => $studentDetails->email,
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with a blank last name.
     */
    public function testCreateStudentWithBlankLastNameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => '',
            'university_id' => $studentDetails->university_id,
            'email' => $studentDetails->email,
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with a blank university ID.
     */
    public function testCreateStudentWithBlankUniversityIdFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => '',
            'email' => $studentDetails->email,
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with an existing university ID.
     */
    public function testCreateStudentWithExistingUniversityIdFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $existingStudent = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $existingStudent->university_id,
            'email' => $studentDetails->email,
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with a blank email address.
     */
    public function testCreateStudentWithBlankEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => '',
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with an invalid email address.
     */
    public function testCreateStudentWithInvalidEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => 'aaa',
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot create a student with an existing email address.
     */
    public function testCreateStudentWithExistingEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $existingStudent = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => $existingStudent->email,
        ];

        $response = $this->post(route('students.store'), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student with a blank first name.
     */
    public function testUpdateStudentWithBlankFirstNameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();
        $newStudentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => '',
            'last_name' => $newStudentDetails->last_name,
            'university_id' => $newStudentDetails->university_id,
            'email' => $newStudentDetails->email,
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student with a blank last name.
     */
    public function testUpdateStudentWithBlankLastNameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();
        $newStudentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $newStudentDetails->first_name,
            'last_name' => '',
            'university_id' => $newStudentDetails->university_id,
            'email' => $newStudentDetails->email,
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student with a blank university ID.
     */
    public function testUpdateStudentWithBlankUniversityIdFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => '',
            'email' => $studentDetails->email,
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student's university ID an existing university ID.
     */
    public function testUpdateStudentWithExistingUniversityIdFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $existingStudent = factory(Student::class)->create();
        $student = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $existingStudent->university_id,
            'email' => $studentDetails->email,
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student with a blank email address.
     */
    public function testUpdateStudentWithBlankEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => '',
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student with an invalid email address.
     */
    public function testUpdateStudentWithInvalidEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => 'aaa',
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot update a student's email address to an existing email address.
     */
    public function testUpdateStudentWithExistingEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $existingStudent = factory(Student::class)->create();
        $student = factory(Student::class)->create();
        $studentDetails = factory(Student::class)->make();

        $data = [
            'first_name' => $studentDetails->first_name,
            'last_name' => $studentDetails->last_name,
            'university_id' => $studentDetails->university_id,
            'email' => $existingStudent->email,
        ];

        $response = $this->patch(route('students.update', ['student' => $student->id]), $data);
        $response->assertRedirect();
    }

    /**
     * Assert that a student that has been deactivate cannot be deactivated again.
     */
    public function testDeactivateStudentThatIsAlreadyDeactivated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $student = factory(Student::class)->create([
            'active' => false,
        ]);

        $response = $this->delete(route('students.deactivate', ['student' => $student->id]));
        $response->assertRedirect();
    }
}
