<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class WebRouteTest
 * @package Tests\Feature
 */
class WebRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example when unauthenticated.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get(route('home'));

        $response->assertRedirect();
    }
}
