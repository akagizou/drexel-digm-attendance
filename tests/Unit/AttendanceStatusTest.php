<?php

namespace Tests\Unit;

use App\AttendanceStatus;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class AttendanceStatusTest
 *
 * @package Tests\Unit
 */
class AttendanceStatusTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an attendance status can be created and fetched properly.
     */
    public function testCreateAttendanceStatus()
    {
        $status = factory(AttendanceStatus::class)->create();

        $savedStatus = AttendanceStatus::find($status->id);

        $this->assertEquals($status->name, $savedStatus->name);
        $this->assertEquals($status->display_order, $savedStatus->display_order);
    }

    /**
     * Attempt to create two attendance statuses with the same name.
     */
    public function testCreateTwoAttendanceStatusWithSameName()
    {
        $status1 = factory(AttendanceStatus::class)->create();
        $this->expectException(\PDOException::class);
        factory(AttendanceStatus::class)->create([
            'name' => $status1->name,
        ]);
    }

    /**
     * Attempt to save two attendance statuses with the same name but different casing.
     */
    public function testCreateTwoAttendanceStatusesWithSameNameDifferentCasingIsAccepted()
    {
        // TODO: Find out why a PDOException isn't thrown on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        $status1 = factory(AttendanceStatus::class)->create([
            'name' => 'Here',
        ]);
        factory(AttendanceStatus::class)->create([
            'name' => 'HERE',
        ]);

        $this->expectException(\PDOException::class);
    }

    /**
     * Attempt to create two attendance statuses with similar-looking names.
     * One uses a Latin 'a' and the other uses a Cyrillic 'a'.
     */
    public function testCreateTwoAttendanceStatusesWithSimilarNameButWithDifferentUnicodeCharacters()
    {
        // Has "U+0061 LATIN SMALL LETTER A"
        $statusWithLatin = factory(AttendanceStatus::class)->create([
            'name' => "L\u{0061}te",
        ]);
        // Has "U+0430 CYRILLIC SMALL LETTER A"
        $statusWithCyrillic = factory(AttendanceStatus::class)->create([
            'name' => "L\u{0430}te",
        ]);

        $savedAttendanceStatusWithLatin = AttendanceStatus::find($statusWithLatin->id);
        $savedAttendanceStatusWithCyrillic = AttendanceStatus::find($statusWithCyrillic->id);

        // Assert that the database saved the Unicode character.
        $this->assertEquals($statusWithLatin->name, $savedAttendanceStatusWithLatin->name);
        $this->assertEquals($statusWithCyrillic->name, $savedAttendanceStatusWithCyrillic->name);

        // Assert that the Cyrillic small 'a' is not the same as ASCII 'a'.
        $this->assertNotEquals('Late', $savedAttendanceStatusWithCyrillic->name);
    }

    /**
     * Attempt to create an attendance status with a display_order value that already exists.
     */
    public function testCreateAttendanceStatusDisplayOrderWithSameNumber()
    {
        $status1 = factory(AttendanceStatus::class)->create();
        $this->expectException(\PDOException::class);
        factory(AttendanceStatus::class)->create([
            'display_order' => $status1->display_order,
        ]);
    }
}
