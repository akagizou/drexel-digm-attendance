<?php

namespace Tests\Unit;

use App\Course;
use App\CourseAttendance;
use App\Student;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class CourseAttendanceTest
 *
 * @package Tests\Unit
 */
class CourseAttendanceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an course attendance entry can be created and fetched properly.
     */
    public function testCreateCourseAttendance()
    {
        $attendance = factory(CourseAttendance::class)->create();

        $savedAttendance = CourseAttendance::find($attendance->id);

        $this->assertEquals($attendance->course_id, $savedAttendance->course_id);
        $this->assertEquals($attendance->student_id, $savedAttendance->student_id);
        $this->assertEquals($attendance->user_id, $savedAttendance->user_id);
        $this->assertEquals($attendance->attendance_status_id, $savedAttendance->attendance_status_id);
        $this->assertEquals($attendance->excused, $savedAttendance->excused);
        $this->assertEquals($attendance->note, $savedAttendance->note);
        $this->assertEquals($attendance->created_at, $savedAttendance->created_at);
        $this->assertEquals($attendance->updated_at, $savedAttendance->updated_at);
    }

    /**
     * Fetch a list of attendance records for a course for today.
     */
    public function testGetAttendanceRecordsForToday()
    {
        $course = factory(Course::class)->create();

        $student1 = factory(Student::class)->create();
        $student1->courses()->attach($course);
        $student2 = factory(Student::class)->create();
        $student2->courses()->attach($course);

        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student1->id,
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $course->id,
            'student_id' => $student2->id,
        ]);

        $attendanceRecordsByCourse = $course->courseAttendances()->today()->get();
        $attendanceRecordsByStudent1 = $student1->courseAttendances()->today()->get();
        $attendanceRecordsByStudent2 = $student2->courseAttendances()->today()->get();

        $this->assertCount(2, $attendanceRecordsByCourse);
        $this->assertCount(1, $attendanceRecordsByStudent1);
        $this->assertCount(1, $attendanceRecordsByStudent2);
    }

    /**
     * Test that obtaining attendance records
     * for a course on a specific date fetches the appropriate entries.
     */
    public function testGetAttendanceRecordsForDate()
    {
        $dateString = '1975-04-03';
        $datetime = Carbon::parse($dateString);

        $attendanceRecord1 = factory(CourseAttendance::class)->create([
            'created_at' => $datetime->toDateTimeString(),
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $attendanceRecord1->course_id,
            'created_at' => $datetime->toDateTimeString(),
        ]);
        factory(CourseAttendance::class)->create([
            'course_id' => $attendanceRecord1->course_id,
        ]);

        $attendanceRecordsByDate = CourseAttendance::date($dateString)->where('course_id', $attendanceRecord1->course_id)->get();
        $attendanceRecordsToday = CourseAttendance::today()->where('course_id', $attendanceRecord1->course_id)->get();

        $this->assertCount(2, $attendanceRecordsByDate);
        $this->assertCount(1, $attendanceRecordsToday);
    }

    /**
     * Assert that the formatter method returns a date and time string
     * in 12-hour format, with no timezone passed making it default to UTC.
     */
    public function testGetCreatedAtDateTime12AttributeWithoutTimezonePassed()
    {
        $dateString = '1975-04-03 15:43';
        $datetime = Carbon::parse($dateString);

        $attendanceRecord = factory(CourseAttendance::class)->create([
            'created_at' => $datetime->toDateTimeString(),
        ]);

        $this->assertTrue(Carbon::hasFormat($attendanceRecord->getCreatedAtDateTime12(), 'F d, Y, h:i A'));
        $this->assertEquals('April 03, 1975, 03:43 PM', $attendanceRecord->getCreatedAtDateTime12());
    }

    /**
     * Assert that the formatter method returns a date and time string
     * in 12-hour format in the course's local time.
     */
    public function testGetCreatedAtDateTime12AttributeWithTimezonePassed()
    {
        $datetimeFormat = 'F d, Y, h:i A';
        $dateString = '1975-04-03 15:43';
        $datetime = Carbon::parse($dateString);

        $attendanceRecord = factory(CourseAttendance::class)->create([
            'created_at' => $datetime->toDateTimeString(),
        ]);

        $this->assertTrue(Carbon::hasFormat($attendanceRecord->getCreatedAtDateTime12($attendanceRecord->course->timezone), $datetimeFormat));
        $this->assertEquals($datetime->timezone($attendanceRecord->course->timezone)->format($datetimeFormat), $attendanceRecord->getCreatedAtDateTime12($attendanceRecord->course->timezone));
    }

    /**
     * Assert that the formatter method returns the day of the week (full name)
     * for the created_at field with no timezone passed, making it default to UTC.
     */
    public function testGetCreatedAtDayOfWeekAttributeWithoutTimezonePassed()
    {
        $dateString = '1975-04-03 15:43';
        $datetime = Carbon::parse($dateString);

        $attendanceRecord = factory(CourseAttendance::class)->create([
            'created_at' => $datetime->toDateTimeString(),
        ]);

        $this->assertTrue(Carbon::hasFormat($attendanceRecord->getCreatedAtDayOfWeek(), 'l'));
        $this->assertEquals('Thursday', $attendanceRecord->getCreatedAtDayOfWeek());
    }

    /**
     * Assert that the formatter method returns the day of the week (full name)
     * for the created_at field in the course's local time.
     */
    public function testGetCreatedAtDayOfWeekAttributeWithTimezonePassed()
    {
        $datetimeFormat = 'l';
        $dateString = '1975-04-03 15:43';
        $datetime = Carbon::parse($dateString);

        $attendanceRecord = factory(CourseAttendance::class)->create([
            'created_at' => $datetime->toDateTimeString(),
        ]);

        $this->assertTrue(Carbon::hasFormat($attendanceRecord->getCreatedAtDayOfWeek($attendanceRecord->course->timezone), $datetimeFormat));
        $this->assertEquals($datetime->timezone($attendanceRecord->course->timezone)->format($datetimeFormat), $attendanceRecord->getCreatedAtDayOfWeek($attendanceRecord->course->timezone));
    }

    /**
     * Assert that the formatter method returns the day of the week (full name)
     * for the created_at field in the course's local time, which is on the next day.
     */
    public function testGetCreatedAtDayOfWeekAttributeWithTimezonePassedAndTimezoneGoesToNextDay()
    {
        $datetimeFormat = 'l';
        $dateString = '1975-04-03 22:43';
        // Use an integer rather than a string since timezone regions come and go.
        $timezoneOffset = new \DateTimeZone('+5');
        $datetime = Carbon::parse($dateString);

        $attendanceRecord = factory(CourseAttendance::class)->create([
            'created_at' => $datetime->toDateTimeString(),
        ]);

        $this->assertEquals('Friday', $attendanceRecord->getCreatedAtDayOfWeek($timezoneOffset));
        $this->assertEquals($datetime->timezone($timezoneOffset)->format($datetimeFormat), $attendanceRecord->getCreatedAtDayOfWeek($timezoneOffset));
    }
}
