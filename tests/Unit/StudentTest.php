<?php

namespace Tests\Unit;

use App\Student;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
Use \PDOException;

/**
 * Class StudentTest
 *
 * @package Tests\Unit
 */
class StudentTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an student can be created and fetched properly.
     */
    public function testCreateStudent()
    {
        $student = factory(Student::class)->create();

        $savedStudent = Student::find($student->id);

        $this->assertEquals($student->first_name, $savedStudent->first_name);
        $this->assertEquals($student->last_name, $savedStudent->last_name);
        $this->assertEquals($student->university_id, $savedStudent->university_id);
        $this->assertEquals($student->email, $savedStudent->email);
    }

    /**
     * Attempt to create two students with the same university ID.
     */
    public function testCreateTwoStudentsWithSameUniversityId()
    {
        $student1 = factory(Student::class)->create();
        $this->expectException(\PDOException::class);
        factory(Student::class)->create([
            'university_id' => $student1->university_id,
        ]);
    }

    /**
     * Attempt to create two students with the same email but differing casing.
     */
    public function testCreateTwoStudentsWithEmailsHavingDifferentCasing()
    {
        // TODO: Find out why a PDOException is not thrown on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        $student1 = factory(Student::class)->create([
            'email' => 'example@example.com',
        ]);
        factory(Student::class)->create([
            'email' => 'EXAMPLE@example.com',
        ]);

        $this->expectException(\PDOException::class);
    }

    /**
     * Attempt to create two students with similar-looking emails.
     * One uses a Latin 'a' and the other uses a Cyrillic 'a'.
     */
    public function testCreateTwoStudentsWithSimilarEmailButWithDifferentUnicodeCharacters()
    {
        // Has "U+0061 LATIN SMALL LETTER A"
        $studentWithLatin = factory(Student::class)->create([
            'email' => "j\u{0061}ne_doe@example.com",
        ]);
        // Has "U+0430 CYRILLIC SMALL LETTER A"
        $studentWithCyrillic = factory(Student::class)->create([
            'email' => "j\u{0430}ne_doe@example.com",
        ]);

        $savedStudentWithLatin = Student::find($studentWithLatin->id);
        $savedStudentWithCyrillic = Student::find($studentWithCyrillic->id);

        // Assert that the email with the Unicode character was saved correctly.
        $this->assertNotEquals($studentWithLatin->email, $studentWithCyrillic->email);
        $this->assertNotEquals($savedStudentWithLatin->email, $savedStudentWithCyrillic->email);

        $this->assertNotEquals('jane_doe@example.com', $savedStudentWithCyrillic->email);
    }

    /**
     * Assert that the model has a full_name attribute accessor
     * that formats the first and last name into a single string,
     * separated by a single space between them.
     */
    public function testGetFullNameAttribute()
    {
        $user = factory(Student::class)->create();

        $expectedFullName = $user->first_name . ' ' . $user->last_name;
        $this->assertEquals($expectedFullName, $user->full_name);
    }
}
